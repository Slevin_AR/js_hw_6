/*
const worker = {
    name: "Oleg",
    surname: "Ozavich",
    rate: 400,
    days: 18,

    getSalary: function() {
        document.write(`<p>Monthly salary : ${this.reat * this.days} <hr>`);
    }
}



for ( let element in worker) {
    document.write(`<p> ${worker[element]}`);
}

Не корекно виводить метод 


document.write(`<p> Name : ${worker.name}`);
document.write(`<p> Surname : ${worker.surname}`);
document.write(`<p> Reat : ${worker.rate}`);
document.write(`<p> Days : ${worker.days}`);
worker.getSalary();
*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Worker {

    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;      
    }

    getSalary() {
        document.write(`<p>Monthly salary : ${this.rate * this.days} <hr>`);
    }
}

const user1 = new Worker(`Oleg`, "Ozavich", 400, 18);
document.write(`<p> Name : ${user1.name}`);
document.write(`<p> Surname : ${user1.surname}`);
document.write(`<p> Reat : ${user1.rate}`);
document.write(`<p> Days : ${user1.days}`);
user1.getSalary(); 

/////////////////////////////////////////////////////////////////////////////////

class MyString {

    constructor(rowText) {
        this.rowText = rowText;  
    }

    reverse() {
        return this.rowText.split(``).reverse().join(``);
    }

    ucFirst(){
       return this.rowText.charAt(0).toUpperCase() + this.rowText.slice(1)
    }

    ucWords(){
        const words = this.rowText.split(` `);
        const result = [];
        for (let i=0; i < words.length; i++) {
            result[i] = words[i][0].toUpperCase() + words[i].slice(1);
        }

        return result.join(` `);
     }
}
const text1 = new MyString("oleg is a good person");
document.write(`<p>`+ text1.reverse());
document.write(`<p>`+ text1.ucFirst());
document.write(`<p>`+ text1.ucWords() + '<hr>');

document.write('<p> Перебір тексту якщо не використовувати .split(` `) '); 
const forTest21 = ('this is our text')
function letitbe(arg) {
    const text = arg.split('');

    for (let i=0; i < text.length; i++) {
        if ( i === 0) {
            text[i] = text[i].toUpperCase(); 
        }
        if (text[i] === " " ) {
            text[i+1] = text[i+1].toUpperCase();
        }
    }
    return text.join('')
}

letitbe(forTest21);
document.write('<br>'+ letitbe(forTest21));

document.write('<hr>'); 

/////////////////////////////////////////////////////////////////////////////////

class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    docWrt() {
        return document.write(`<p>`+ this.number + ' ' + this.model + ' ' + this.weight );
    }
    receiveCall(name) {
        return document.write(`<p> Телефонує `+ name);
    }
    getNumber() {
        return document.write(`<p>`+ this.number );
    }
}

const phone1 = new Phone ( 432423423 ,'Nokia', 100 );
const phone2 = new Phone ( 854723423 ,'LG', 90 );
const phone3 = new Phone ( 124346767 ,'Samsung', 120 );

phone1.docWrt();
phone2.docWrt();
phone3.docWrt();
phone1.receiveCall('Oleg');
phone2.receiveCall('Oleg');
phone3.receiveCall('Oleg');
phone1.getNumber();
phone2.getNumber();
phone3.getNumber();
document.write('<hr>'); 

/////////////////////////////////////////////////////////////////////////////////


class Driver {
    constructor(fullName, exp ) {
        this.fullName = fullName;
        this.exp = exp;
    }
}


class Engine {
    constructor(power , producer ) {
        this.power = power;
        this.producer = producer;
    }
}

class Car {
    constructor(carBrand, carClass, weight, driver, engine) {
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }
    start() {
        document.write("Поїхали");
    }
    stop() {
        document.write("Зупиняємося");
    }
    turnRight() {
        document.write("Поворот праворуч");
    }
    turnLeft() {
        document.write("Поворот ліворуч");
    }
    toString() {
        return this.carBrand + ' ' + this.carClass + ' ' 
        + this.weight + ' ' + this.driver.fullName + ' ' + this.driver.fullName + ' '
        + this.driver.exp + ' ' + this.engine.power + ' ' + this.engine.producer ;
    }
}

class Lorry extends Car {
        constructor (carrying, carBrand, carClass, weight, driver, engine )  {
            super(carBrand, carClass, weight, driver, engine);
            this.carrying = carrying;
        }
}
class SportCar extends Car {
    constructor (speed, carBrand, carClass, weight, driver, engine )  {
        super(carBrand, carClass, weight, driver, engine);
        this.speed = speed;
    }
}

const driver1 = new Driver('Oleg V V ', '5 years');
const engine1 = new Engine('400horsepower', 'Volvo');
const car1 = new Car ( 'Nissan' ,'ralli', '34t', driver1, engine1 );
const car2 = new Lorry ( '20Tonn','Nissan' ,'ralli', '34t', driver1, engine1 );
const car3 = new SportCar ( '240km/h', 'Nissan' ,'ralli', '34t', driver1, engine1 );

document.write(`<p>` + car1.toString());
document.write(`<p>` + car2.toString() + ' ' + car2.carrying);
document.write(`<p>` + car3.toString() + ' ' + car3.speed);

document.write('<hr>');
/////////////////////////////////////////////////////////////////////////////////

class Animal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
    }
    makeNoise() {
        document.write("Така тварина подає голос");
    }
    eat() {
        document.write("Така тварина їсть яблука");
    }
    sleep() {
        document.write("Така тварина спить");
    }
}
class Dog extends Animal {
    constructor (food, location, color)  {
        super(food, location);
        this.color = color;
    }
    makeNoise() {
        document.write("Собака подає голос");
    }
    eat() {
        document.write("Собака їсть м'ясо");
    }
}
class Cat extends Animal {
    constructor (food, location, breed )  {
        super(food, location);
        this.breed = breed;
    }
    makeNoise() {
        document.write("Котик мяукає");
    }
    eat() {
        document.write("Котик їсть рибку");
    }
}
class Horse extends Animal {
    constructor (food, location , speed )  {
        super(food, location);
        this.speed = speed;
    }
    makeNoise() {
        document.write("Кінь ігогоче");
    }
    eat() {
        document.write("Кінь їсть солому");
    }
}

class Veterenar {

    treatAnimal(animal){
        document.write('<p> Тварина, що на прийомі їсть ' + animal.food + ' з місця ' + animal.location );
    }
    main(animals){
        animals.forEach(animal => {
            this.treatAnimal(animal);
        });
    }
}

const vet1 = new Veterenar();
const cat1 = new Cat('fish', 'Kyiv', 'Britan');
const cat2 = new Cat('dry food', 'Odessa', 'street');
const dog1 = new Dog('dry food', 'Harkiv', 'dark ');
const horse1 = new Horse('hay', 'Herson', '60km/h ');
const horse2 = new Horse('straw', 'Dnipro', '70km/h ');
vet1.treatAnimal(cat1);
document.write('<br>');
document.write('<br>');
vet1.main([cat1,cat2,dog1,horse1,horse2]);

document.write('<hr>');
/////////////////////////////////////////////////////////////////////////////////




